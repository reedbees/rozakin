package ir.reedbees.rozakindergarten.config;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.web.util.matcher.AndRequestMatcher;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.sql.DataSource;

@Configuration
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private DataSource dataSource;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication().dataSource(dataSource)
                .passwordEncoder(NoOpPasswordEncoder.getInstance())
                .usersByUsernameQuery("select national_id,last_name,enable from kids where national_id=?")
                .authoritiesByUsernameQuery("select national_id,roles from authorities where national_id=?");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .authorizeRequests().antMatchers("/", "/index","/registerParent","/reg","/parent/reg","/parent/..","/kids/..","/kids/reg","/registerKids","/continue","/parent/registerKids","/parent/*","/feedback","/contact","/gallery","/images/**","/gamecontroller/getall","/ter","/gamecontroller/reg").permitAll()
                .and().authorizeRequests().antMatchers("/admin").hasAnyAuthority("معلم")
                .anyRequest().authenticated()
//                .and().authorizeRequests().antMatchers("/test").hasAnyAuthority("کودک", "معلم")
                .and().formLogin()
                .loginPage("/login").usernameParameter("national_id").passwordParameter("last_name")
                .permitAll().and().logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout")).logoutSuccessUrl("/loger").permitAll();
    }
}
