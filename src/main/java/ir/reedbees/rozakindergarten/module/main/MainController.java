package ir.reedbees.rozakindergarten.module.main;

import ir.reedbees.rozakindergarten.email.EmailCfg;
import ir.reedbees.rozakindergarten.email.FeedBack;
import ir.reedbees.rozakindergarten.email.controller.FeedBackController;
import ir.reedbees.rozakindergarten.helpDate.DayCounter;
import ir.reedbees.rozakindergarten.module.parent.service.ParentService;
import ir.reedbees.rozakindergarten.module.payment.entity.Payment;
import ir.reedbees.rozakindergarten.module.payment.service.PaymentService;
import ir.reedbees.rozakindergarten.module.users.entity.Kids;
import ir.reedbees.rozakindergarten.module.users.service.KidsService;
import ir.reedbees.rozakindergarten.others.DateCalculator;
import ir.reedbees.rozakindergarten.others.DateString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.xml.bind.ValidationException;
import java.security.Principal;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Calendar;

import static java.time.temporal.ChronoUnit.DAYS;

@Controller
public class MainController {
    private KidsService kidsService;
    private ParentService parentService;
    private PaymentService paymentService;


    @Autowired
    public MainController(KidsService kidsService, ParentService parentService, PaymentService paymentService) {
        this.kidsService = kidsService;
        this.parentService = parentService;
        this.paymentService = paymentService;
    }

    @RequestMapping
    public String index() {
        return "index";
    }


    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String test2() {
        return "login";
    }


    @RequestMapping(value = "/kids/test",method = RequestMethod.GET)
    public String teer() {
        return "usertemplate";
    }

    @RequestMapping(value = "/loger",method = RequestMethod.GET)
    public String logout() {
        return "loger";
    }

    @RequestMapping(value = "/test", method = RequestMethod.GET)
    public String test3(Model model, Principal principal) {
//        String str = principal.getName();
//        model.addAttribute("kids", kidsService.findByNationalId(Long.parseLong(str)));
//        Kids kid = (Kids) kidsService.findByNationalId(Long.parseLong(str));
//        model.addAttribute("payment",kid.getPayments());
//        DayCounter dayCounter = new DayCounter();
//        long dayB;
//        dayB = dayCounter.dayCount(kid.getPayments().get(kid.getPayments().size() - 1).getDays());
//        model.addAttribute("dayCounter",dayCounter);
//        System.out.println(principal.getName());
//        return "usertemplate";



        // now every thing is okey except day counter
        String str = principal.getName();
        model.addAttribute("kids",kidsService.findByNationalId(Long.parseLong(str)));
        // kidsService.findByNationalId(principal.getName());
//        System.out.println(principal.getName());
        return "usertemplate";
    }

    @RequestMapping(value = "/registerParent", method = RequestMethod.GET)
    public String test4() {
        return "registerParent";
    }

    @RequestMapping(value = "/ter",method = RequestMethod.GET)
    public String ter() {
        return "check";
    }

    @RequestMapping(value = "/parent/registerKids", method = RequestMethod.GET)
    public String test5(Model model) {
        model.addAttribute("parent", parentService.findAllParent());
        //  System.out.printf("Fuck you guys");
        return "registerKids";
        // return "registerKids";
    }

    @RequestMapping(value = "/admin", method = RequestMethod.GET)
    public String admin(Model model) {
        model.addAttribute("payment", paymentService.findAllPayment());
        model.addAttribute("kids", kidsService.findAllKids());
        return "admin";
    }

    @RequestMapping(value = "/pay1", method = RequestMethod.GET)
    public String pay1(Model model, Principal principal) throws ValidationException {
        String str = principal.getName();
        Kids kids = (Kids) kidsService.findByNationalId(Long.parseLong(str));
        System.out.println("now we wanna test the correct date time of now");
        LocalDate today = LocalDate.now();
        System.out.println(today);
        //today.plusMonths(2);
        System.out.println(today.plusMonths(2));


        if (kids.getPayments().size() == 0) {
            Payment payment = new Payment();

            payment.setCallBack("er");
            payment.setFinished(true);
            payment.setCash(true);
            payment.setDays(today.plusMonths(1));
            payment.setId("2");
            payment.setAmount(4);
            payment.setKids(kids);
            payment.setTrackId(2);
            payment.setLink("ss");
            paymentService.register(payment);
            System.out.println(payment.toString());
//            EmailCfg emailCfg = setThem();
//            FeedBackController feedBackController = new FeedBackController(emailCfg);
//            FeedBack feedBack = new FeedBack();
//            feedBack.setName(kids.getName());
//            feedBack.setEmail("es.erfan95@gmail.com");
//            String feed = kids.getName().concat("پرداخت خود را انجام داد").concat("به مبلغ").concat(Integer.toString(payment.getAmount()));
//            feedBack.setFeedback(feed);
//            feedBackController.sendFeedback(feedBack);
        }else {
            long dayBetween = DAYS.between(kids.getPayments().get(kids.getPayments().size() - 1).getDays(),today);
            Payment payment = new Payment();
            payment.setCallBack("er");
            payment.setFinished(true);
            payment.setCash(true);
            LocalDate today2 = LocalDate.now().plusDays(dayBetween);
            payment.setDays(today2.plusMonths(1));
            payment.setId("2");
            payment.setAmount(4);
            payment.setKids(kids);
            paymentService.register(payment);
//            EmailCfg emailCfg = setThem();
//            FeedBackController feedBackController = new FeedBackController(emailCfg);
//            FeedBack feedBack = new FeedBack();
//            feedBack.setName(kids.getName());
//            feedBack.setEmail("es.erfan95@gmail.com");
//            String feed = kids.getName().concat("پرداخت خود را انجام داد").concat("به مبلغ").concat(Integer.toString(payment.getAmount()));
//            feedBack.setFeedback(feed);
//            feedBackController.sendFeedback(feedBack);
        }



        return "index";
    }

    @RequestMapping(value = "/pay2",method = RequestMethod.GET)
    public String pay2(Model model ,Principal principal ) throws ValidationException {
        String str = principal.getName();
        Kids kids = (Kids) kidsService.findByNationalId(Long.parseLong(str));
        LocalDate today = LocalDate.now();

        if (kids.getPayments().size() == 0) {
            Payment payment = new Payment();
           // payment.setOrderId((long) 232);
            payment.setCallBack("er");
            payment.setFinished(true);
            payment.setCash(true);
            payment.setDays(today.plusMonths(3));
            payment.setId("2");
            payment.setAmount(4);
            payment.setKids(kids);
//            payment.setDays(LocalDate.now());
            payment.setTrackId(2);
            payment.setLink("ss");
//            kids.setNewPayment(payment);
            paymentService.register(payment);
            System.out.println(payment.toString());
//            EmailCfg emailCfg = setThem();
//            FeedBackController feedBackController = new FeedBackController(emailCfg);
//            FeedBack feedBack = new FeedBack();
//            feedBack.setName(kids.getName());
//            feedBack.setEmail("es.erfan95@gmail.com");
//            String feed = kids.getName().concat("پرداخت خود را انجام داد").concat("به مبلغ").concat(Integer.toString(payment.getAmount()));
//            feedBack.setFeedback(feed);
//            feedBackController.sendFeedback(feedBack);
        }else {
            long dayBetween = DAYS.between(kids.getPayments().get(kids.getPayments().size() - 1).getDays(),today);
            Payment payment = new Payment();
            payment.setCallBack("er");
            payment.setFinished(true);
            payment.setCash(true);
            LocalDate today2 = LocalDate.now().plusDays(dayBetween);
            payment.setDays(today2.plusMonths(3));
            payment.setId("2");
            payment.setAmount(4);
            payment.setKids(kids);
            paymentService.register(payment);
//            EmailCfg emailCfg = setThem();
//            FeedBackController feedBackController = new FeedBackController(emailCfg);
//            FeedBack feedBack = new FeedBack();
//            feedBack.setName(kids.getName());
//            feedBack.setEmail("es.erfan95@gmail.com");
//            String feed = kids.getName().concat("پرداخت خود را انجام داد").concat("به مبلغ").concat(Integer.toString(payment.getAmount()));
//            feedBack.setFeedback(feed);
//            feedBackController.sendFeedback(feedBack);
        }


        return "index";
    }

//    public EmailCfg setThem() {
////        EmailCfg emailCfg = new EmailCfg();
////        emailCfg.setHost("smtp.mailtrap.io");
////        emailCfg.setPort(2525);
////        emailCfg.setUsername("f9ae8b6df5569b");
////        emailCfg.setPassword("3c66c5f673f5bb");
////        return emailCfg;
//    }

    @RequestMapping(value = "/pay3",method = RequestMethod.GET)
    public String pay3(Model model ,Principal principal ) throws ValidationException {
        String str = principal.getName();
        Kids kids = (Kids) kidsService.findByNationalId(Long.parseLong(str));
        LocalDate today = LocalDate.now();
        if (kids.getPayments().size() == 0) {
            Payment payment = new Payment();
            // payment.setOrderId((long) 232);
            payment.setCallBack("er");
            payment.setFinished(true);
            payment.setCash(true);
            payment.setDays(today.plusMonths(6));
            payment.setId("2");
            payment.setAmount(4);
            payment.setKids(kids);
//            payment.setDays(LocalDate.now());
            payment.setTrackId(2);
            payment.setLink("ss");
//            kids.setNewPayment(payment);
            paymentService.register(payment);
            System.out.println(payment.toString());
//            EmailCfg emailCfg = setThem();
//            FeedBackController feedBackController = new FeedBackController(emailCfg);
//            FeedBack feedBack = new FeedBack();
//            feedBack.setName(kids.getName());
//            feedBack.setEmail("es.erfan95@gmail.com");
//            String feed = kids.getName().concat("پرداخت خود را انجام داد").concat("به مبلغ").concat(Integer.toString(payment.getAmount()));
//            feedBack.setFeedback(feed);
//            feedBackController.sendFeedback(feedBack);
        }else {
            long dayBetween = DAYS.between(kids.getPayments().get(kids.getPayments().size() - 1).getDays(),today);
            Payment payment = new Payment();
            payment.setCallBack("er");
            payment.setFinished(true);
            payment.setCash(true);
            LocalDate today2 = LocalDate.now().plusDays(dayBetween);
            payment.setDays(today2.plusMonths(6));
            payment.setId("2");
            payment.setAmount(4);
            payment.setKids(kids);
            paymentService.register(payment);
//            EmailCfg emailCfg = setThem();
//            FeedBackController feedBackController = new FeedBackController(emailCfg);
//            FeedBack feedBack = new FeedBack();
//            feedBack.setName(kids.getName());
//            feedBack.setEmail("es.erfan95@gmail.com");
//            String feed = kids.getName().concat("پرداخت خود را انجام داد").concat("به مبلغ").concat(Integer.toString(payment.getAmount()));
//            feedBack.setFeedback(feed);
//            feedBackController.sendFeedback(feedBack);
        }
        return "index";
    }


    @RequestMapping(value = "/pay4",method = RequestMethod.GET)
    public String pay4(Model model, Principal principal) throws ValidationException {
        String str = principal.getName();
        Kids kids = (Kids) kidsService.findByNationalId(Long.parseLong(str));
        LocalDate today = LocalDate.now();
        if (kids.getPayments().size() == 0) {
            Payment payment = new Payment();
            // payment.setOrderId((long) 232);
            payment.setCallBack("er");
            payment.setFinished(true);
            payment.setCash(true);
            payment.setDays(today.plusMonths(12));
            payment.setId("2");
            payment.setAmount(4);
            payment.setKids(kids);
            payment.setTrackId(2);
            payment.setLink("ss");
            paymentService.register(payment);
            System.out.println(payment.toString());
//            EmailCfg emailCfg = setThem();
//            FeedBackController feedBackController = new FeedBackController(emailCfg);
//            FeedBack feedBack = new FeedBack();
//            feedBack.setName(kids.getName());
//            feedBack.setEmail("es.erfan95@gmail.com");
//            String feed = kids.getName().concat("پرداخت خود را انجام داد").concat("به مبلغ").concat(Integer.toString(payment.getAmount()));
//            feedBack.setFeedback(feed);
//            feedBackController.sendFeedback(feedBack);
        }else {
            long dayBetween = DAYS.between(kids.getPayments().get(kids.getPayments().size() - 1).getDays(),today);
            Payment payment = new Payment();
            payment.setCallBack("er");
            payment.setFinished(true);
            payment.setCash(true);
            LocalDate today2 = LocalDate.now().plusDays(dayBetween);
            payment.setDays(today2.plusMonths(12));
            payment.setId("2");
            payment.setAmount(4);
            payment.setKids(kids);
            paymentService.register(payment);
//            EmailCfg emailCfg = setThem();
//            FeedBackController feedBackController = new FeedBackController(emailCfg);
//            FeedBack feedBack = new FeedBack();
//            feedBack.setName(kids.getName());
//            feedBack.setEmail("es.erfan95@gmail.com");
//            String feed = kids.getName().concat("پرداخت خود را انجام داد").concat("به مبلغ").concat(Integer.toString(payment.getAmount()));
//            feedBack.setFeedback(feed);
//            feedBackController.sendFeedback(feedBack);
        }
        return "index";
    }

    @RequestMapping(value = "/contact",method = RequestMethod.GET)
    public String contact() {
        return "contacus";
    }

    @RequestMapping(value = "/hand",method = RequestMethod.GET)
    public String hand(Model model) {
        model.addAttribute("kids",kidsService.findAllKids());
        return "HandPayment";
    }

    @RequestMapping(value = "/gallery",method = RequestMethod.GET)
    public String gallery() {
        return "gallelry";
    }
}
