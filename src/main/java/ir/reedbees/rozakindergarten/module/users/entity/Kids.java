package ir.reedbees.rozakindergarten.module.users.entity;


import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import ir.reedbees.rozakindergarten.enums.Roles;
import ir.reedbees.rozakindergarten.module.parent.entity.Parent;
import ir.reedbees.rozakindergarten.module.payment.entity.Payment;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;


@Getter
@Setter
@Entity
@Table(name = "kids")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,property = "nationalId")
public class Kids implements Serializable {

    @Id
    private long nationalId;

    private String name;
    private String lastName;


    //private LocalDate birthday = LocalDate.of(1960, Month.JANUARY, 1);
    // LocalDate birthdate = new LocalDate (1970, 1, 20);      //Birth date
    //   LocalDate now = new LocalDate();                        //Today's date

    // Period period = new Period(birthdate, now, PeriodType.yearMonthDay());


    private int age;


    private Boolean enable = true;

    @ElementCollection(targetClass = Roles.class)
    @CollectionTable(name = "authorities" ,joinColumns = @JoinColumn(name = "nationalId",referencedColumnName = "nationalId"))
    @Enumerated(EnumType.STRING)
    private List<Roles> roles;






    private LocalDate register;


//    @ManyToOne(cascade = )
    @ManyToOne
    private Parent parent;


    @OneToMany(mappedBy = "kids")
    private List<Payment> payments;



    public Kids() {
    }

//    public Payment getPayments() {
//        return payments.get(payments.size() - 1);
//    }

    public Payment str() {
        return payments.get(payments.size() - 1);
    }

    public void setRegister(LocalDate register) {
        this.register = register;
    }

    public Kids(long nationalId, String name, String lastName, int birthday, Parent parent) {
        this.nationalId = nationalId;
        this.name = name;
        this.lastName = lastName;
        this.age = birthday;
        this.register = LocalDate.now();
        this.parent = parent;
    }
    public Kids(long nationalId, String name, String lastName, int birthday) {
        this.nationalId = nationalId;
        this.name = name;
        this.lastName = lastName;
        this.age = birthday;
        this.register = LocalDate.now();
    }

    public Kids(long nationalId, String name, String lastName, int birthday, Parent parent, List<Payment> payments) {
        this.nationalId = nationalId;
        this.name = name;
        this.lastName = lastName;
        this.age = birthday;
        this.register = LocalDate.now();
       // this.lastPay = lastPay;
        this.parent = parent;
        this.payments = payments;
    }
    public Kids(long nationalId, String name, String lastName, int birthday, List<Payment> payments) {
        this.nationalId = nationalId;
        this.name = name;
        this.lastName = lastName;
        this.age = birthday;
        this.register = LocalDate.now();
        //this.lastPay = lastPay;
        this.payments = payments;
    }

    public void setNewPayment(Payment payment) {
        payments.add(payment);
    }

}
