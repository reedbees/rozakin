package ir.reedbees.rozakindergarten.module.users.repository;

import ir.reedbees.rozakindergarten.module.users.entity.Kids;
import org.springframework.data.jpa.repository.JpaRepository;

public interface KidsRepository extends JpaRepository<Kids, String> {
    Kids findByNationalId(Long id);
}
