package ir.reedbees.rozakindergarten.module.users.service;

import ir.reedbees.rozakindergarten.module.users.entity.Kids;
import ir.reedbees.rozakindergarten.module.users.repository.KidsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class KidsService {
    private KidsRepository kidsRepository;


    @Autowired
    public KidsService(KidsRepository kidsRepository) {
        this.kidsRepository = kidsRepository;
    }

    public Kids registerKids(Kids kids) {
        kids.setRegister(LocalDate.now());
        return this.kidsRepository.save(kids);
    }

    public List<Kids> findAllKids() {
        return this.kidsRepository.findAll();
    }


    public Object findByNationalId(long name) {
       // System.out.println(kidsRepository.findByNationalId(name));
       return kidsRepository.findByNationalId(name);
    }
}
