package ir.reedbees.rozakindergarten.module.users.controller;

import ir.reedbees.rozakindergarten.module.parent.entity.Parent;
import ir.reedbees.rozakindergarten.module.parent.service.ParentService;
import ir.reedbees.rozakindergarten.module.users.entity.Kids;
import ir.reedbees.rozakindergarten.module.users.service.KidsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.security.Principal;
import java.util.List;


@Controller
@RequestMapping("/kids")
public class KidsController {
    private KidsService kidsService;
    private ParentService parentService;

    @Autowired
    public KidsController(KidsService kidsService) {
        this.kidsService = kidsService;
    }

    //must be complete controller method


    @GetMapping(value = "/getall")
    public @ResponseBody
    List<Kids> getAllKids() {
        return this.kidsService.findAllKids();
    }

    @PostMapping(value = "/register")
    public @ResponseBody
    Kids registerKids(@RequestBody Kids kids) throws IOException {
        return this.kidsService.registerKids(kids);
    }

    @RequestMapping(value = "/reg",method = RequestMethod.POST)
    public String registerParent2(@ModelAttribute Kids kids) {
        kidsService.registerKids(kids);
//        System.out.println("Fuuckkkkkkkkkkk this is fucking sheeeeeeet i hate youyyyyyyyyghhhh" +
//                "****************");
        return "index";
    }

}
