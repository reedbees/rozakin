package ir.reedbees.rozakindergarten.module.game.service;


import ir.reedbees.rozakindergarten.module.game.entity.Game;
import ir.reedbees.rozakindergarten.module.game.repository.GameRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class GameService {
    private GameRepository gameRepository;

    @Autowired
    public GameService(GameRepository gameRepository) {
        this.gameRepository = gameRepository;
    }

    public Game registerGame(Game game) {
        return this.gameRepository.save(game);
    }

    public List<Game> findAllGame() {
        return this.gameRepository.findAll();
    }

    public boolean findById(long id) {
       Optional<Game> game = gameRepository.findById(id);
        return game.isPresent();
    }
}
