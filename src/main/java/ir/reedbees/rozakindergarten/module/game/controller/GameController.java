package ir.reedbees.rozakindergarten.module.game.controller;


import ir.reedbees.rozakindergarten.module.game.entity.Game;
import ir.reedbees.rozakindergarten.module.game.handle.GameHandler;
import ir.reedbees.rozakindergarten.module.game.service.GameService;
import ir.reedbees.rozakindergarten.module.users.entity.Kids;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/gamecontroller")
public class GameController {
    private GameService gameService;

    @Autowired
    public GameController(GameService gameService) {
        this.gameService = gameService;
    }


    @GetMapping(value = "/getall")
    public @ResponseBody
    List<Game> getAllGame() {
        GameHandler gameHandler = new GameHandler();
        System.out.println("this is game handler");
        gameHandler.codeAdder(gameService);
        return this.gameService.findAllGame();
    }


    @RequestMapping(value = "/reg",method = RequestMethod.POST)
    public String registerGame(@ModelAttribute Game game, Model model) {

        if (gameService.findById(game.getId())){
            model.addAttribute("game",game);
            return "cont2";
        }
        return "errr2";
    }
}
