package ir.reedbees.rozakindergarten.module.parent.repository;

import ir.reedbees.rozakindergarten.module.parent.entity.Parent;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ParentRopsitory extends JpaRepository<Parent, Long> {

}
