package ir.reedbees.rozakindergarten.module.parent.service;

import ir.reedbees.rozakindergarten.module.parent.entity.Parent;
import ir.reedbees.rozakindergarten.module.parent.repository.ParentRopsitory;
import ir.reedbees.rozakindergarten.module.users.entity.Kids;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ParentService {
    private ParentRopsitory parentRopsitory;

    @Autowired
    public ParentService(ParentRopsitory parentRopsitory) {
        this.parentRopsitory = parentRopsitory;
    }


    public Parent registerParent(Parent parent) {
        return this.parentRopsitory.save(parent);
    }

    public List<Parent> findAllParent() {
        return this.parentRopsitory.findAll();
    }
}
