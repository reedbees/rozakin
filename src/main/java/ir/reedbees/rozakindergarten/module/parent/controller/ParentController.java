package ir.reedbees.rozakindergarten.module.parent.controller;

import ir.reedbees.rozakindergarten.module.parent.entity.Parent;
import ir.reedbees.rozakindergarten.module.parent.service.ParentService;
import ir.reedbees.rozakindergarten.module.users.entity.Kids;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@Controller
@RequestMapping("/parent")
public class ParentController {
    private ParentService parentService;

    @Autowired
    public ParentController(ParentService parentService) {
        this.parentService = parentService;
    }

    @GetMapping(value = "/getall")
    public @ResponseBody
    List<Parent> getAllParent() {
        return this.parentService.findAllParent();
    }

    @PostMapping(value = "/register")
    public @ResponseBody Parent registerParent(@RequestBody Parent parent) throws IOException {
        return this.parentService.registerParent(parent);
    }

    @RequestMapping(value = "/reg",method = RequestMethod.POST)
    public String registerParent2(@ModelAttribute Parent parent) {
        parentService.registerParent(parent);
//        System.out.println("Fuuckkkkkkkkkkk this is fucking sheeeeeeet i hate youyyyyyyyyghhhh" +
//                "****************");
        return "continue";
    }


}
