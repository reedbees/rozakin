package ir.reedbees.rozakindergarten.module.parent.entity;


import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import ir.reedbees.rozakindergarten.module.users.entity.Kids;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "Parents")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,property = "nationalId")
public class Parent {
    @Id
    private long nationalId;

    private long motherNationalId;


    private String fatherFirstName;
    private String fatherLastName;
    private String motherFirstName;
    private String motherLastName;



    private String fatherPhoneNumber;
    private String motherPhoneNumber;

//    @OneToOne(mappedBy = "parent")
//    private Kids kids;

    /* this one to one mapped It makes to parent information is only showed in kids part!!
    if we want to change it we must type mapped by kids in kids entity
     */


    /*i want to change the one to one relation to one to many
     */
    @OneToMany(mappedBy = "parent")
    private List<Kids> kids;



    public Parent() {
    }

    public Parent(long nationalId, String fatherFirstName, String motherFirstName) {
        this.nationalId = nationalId;
        this.fatherFirstName = fatherFirstName;
        this.motherFirstName = motherFirstName;
    }

    public Parent(long nationalId, String fatherFirstName, String motherFirstName,List<Kids> kids) {
        this.nationalId = nationalId;
        this.fatherFirstName = fatherFirstName;
        this.motherFirstName = motherFirstName;
        this.kids = kids;
    }

}
