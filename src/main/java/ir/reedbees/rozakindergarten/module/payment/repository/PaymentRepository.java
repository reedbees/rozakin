package ir.reedbees.rozakindergarten.module.payment.repository;

import ir.reedbees.rozakindergarten.module.payment.entity.Payment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PaymentRepository extends JpaRepository<Payment,Long> {

}
