package ir.reedbees.rozakindergarten.module.payment.DTO;

import lombok.Getter;
import lombok.Setter;

import java.sql.Timestamp;

@Getter
@Setter
public class PaymentDTO {
    private String orderId;
    private int amount;
    private String callback;

    /*@ManyToOne
    private Kid kid;*/

    private String id;
    private String link;

    private int status;
    private int trackId;

    private Timestamp date;

}
