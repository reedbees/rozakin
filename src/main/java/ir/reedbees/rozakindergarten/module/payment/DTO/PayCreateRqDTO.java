package ir.reedbees.rozakindergarten.module.payment.DTO;


import ir.reedbees.rozakindergarten.module.users.entity.Kids;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PayCreateRqDTO {
    private Kids kid;
    private int amount;

}
