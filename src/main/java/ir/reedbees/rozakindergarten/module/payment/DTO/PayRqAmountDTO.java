package ir.reedbees.rozakindergarten.module.payment.DTO;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PayRqAmountDTO {
    private int amount;
}
