package ir.reedbees.rozakindergarten.module.payment.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import ir.reedbees.rozakindergarten.module.users.entity.Kids;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Setter
@Getter
@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,property = "orderId")
public class Payment {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long orderId;


    private String callBack;

    private String id;

    private String link;

    @CreationTimestamp
    private Timestamp date;

    private int trackId;

    private int amount;

    private Boolean finished;

    private LocalDate days;

    @ManyToOne
    private Kids kids;

    private boolean cash;



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Payment)) return false;

        Payment payment = (Payment) o;

        if (getAmount() != payment.getAmount()) return false;
        if (getTrackId() != payment.getTrackId()) return false;
        if (getOrderId() != null ? !getOrderId().equals(payment.getOrderId()) : payment.getOrderId() != null)
            return false;
        if (getId() != null ? !getId().equals(payment.getId()) : payment.getId() != null) return false;
        return getDate() != null ? getDate().equals(payment.getDate()) : payment.getDate() == null;
    }

    public Payment(String id,Kids kids,String callBack,int trackId,int amount) {
        this.kids = kids;
        this.amount = amount;
    }

    public Payment() {
    }


    @Override
    public String toString() {
        return "Payment{" +
                "orderId='" + orderId + '\'' +
                ", callBack='" + callBack + '\'' +
                ", id='" + id + '\'' +
                ", link='" + link + '\'' +
                ", date=" + date +
                ", trackId=" + trackId +
                ", amount=" + amount +
                ", finished=" + finished +
                ", days=" + days +
                ", kids=" + kids.getNationalId() +
                '}';
    }
}
