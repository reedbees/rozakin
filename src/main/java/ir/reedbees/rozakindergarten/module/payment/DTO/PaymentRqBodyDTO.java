package ir.reedbees.rozakindergarten.module.payment.DTO;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PaymentRqBodyDTO {
    private String orderId;
    private int amount;
    private String callback;
}
