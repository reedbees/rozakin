package ir.reedbees.rozakindergarten.module.payment.service;


import ir.reedbees.rozakindergarten.module.payment.DTO.OnlinePaymentRsBodyDTO;
import ir.reedbees.rozakindergarten.module.payment.DTO.PayCreateRqDTO;
import ir.reedbees.rozakindergarten.module.payment.DTO.PaymentRqBodyDTO;
import ir.reedbees.rozakindergarten.module.payment.DTO.VerifyRqDTO;
import ir.reedbees.rozakindergarten.module.payment.constants.Constants;
import ir.reedbees.rozakindergarten.module.payment.constants.IdPayStatus;
import ir.reedbees.rozakindergarten.module.payment.constants.Paths;
import ir.reedbees.rozakindergarten.module.payment.entity.Payment;
import ir.reedbees.rozakindergarten.module.payment.repository.PaymentRepository;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONException;
import org.json.JSONObject;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.Map;
import java.util.Optional;


@Service
public class PaymentService {
    private PaymentRepository paymentRepo;
    private ModelMapper modelMapper;

    @Autowired

    public PaymentService(PaymentRepository paymentRepo) {
        this.paymentRepo = paymentRepo;
        modelMapper = new ModelMapper();
    }

    /*public void pay(PayCreateRqDTO dto) throws IOException, JSONException {
        Payment payment = modelMapper.map(dto, Payment.class);
        payment.setFinished(false);
        //payment.setCallback();
        HttpResponse response = create(modelMapper.map(payment, PaymentRqBodyDTO.class));
        if (response == null) {
            paymentRepo.delete(payment);
        } else {
            if (response.getStatusLine().getStatusCode() == Response.Status.CREATED.getStatusCode()) {
                String link = handleSuccessPaymentRs(payment, response);
                //Show link in Front
                //or send buyer automatically to link
            } else {
                handleFailurePaymentRs(response);
                paymentRepo.delete(payment);
            }
        }
    }*/

    public String pay(Payment payment) throws IOException, JSONException {
        payment.setFinished(false);
        payment.setCash(false);
        payment.setCallBack(Paths.callback);

        System.out.println();
        System.out.println(payment.getKids().toString());
        System.out.println();

        HttpResponse response = create(payment);
        if (response == null) {
            paymentRepo.delete(payment);
            return "false";
        } else {
            if (response.getStatusLine().getStatusCode() == Response.Status.CREATED.getStatusCode() ||
                    response.getStatusLine().getStatusCode() == 201) {
                String link = handleSuccessPaymentRs(payment, response);
                //Show link in Front
                //or send buyer automatically to link
                return link;
            } else {
                handleFailurePaymentRs(response);
                paymentRepo.delete(payment);
                return "false";
            }
        }
    }

    private void handleFailurePaymentRs(HttpResponse response) throws JSONException {
        JSONObject myResponse = new JSONObject((Map) response);
        int error = myResponse.getInt("id");
        System.out.println(IdPayStatus.message(IdPayStatus.fromCode(error)));
    }

    private String handleSuccessPaymentRs(Payment payment, HttpResponse response) throws JSONException {
        JSONObject myResponse = new JSONObject((Map) response);
        payment.setId(myResponse.getString("id"));
        String link = myResponse.getString("link");
        payment.setLink(link);
        return link;
    }

    /*private HttpResponse create(PaymentRqBodyDTO dto) {
        HttpClient httpClient = HttpClientBuilder.create().build();
        try {
            HttpPost request = new HttpPost(Paths.webTarget + Paths.payment);
            StringEntity params = new StringEntity("{" +
                    "\"" + Constants.oderId + "\": \"" + dto.getOrderId() + "\"" +
                    "\"" + Constants.amount + "\": " + dto.getAmount() +
                    "\"" + Constants.callbackURL + "\": \"" + dto.getCallback() + "\"" +
                    "} ");
            request.setEntity(params);
            request.addHeader("content-type", "application/json");
            request.addHeader(Constants.apiKey, Constants.ApiKey);
            request.addHeader(Constants.sandBox, String.valueOf(true));
            return httpClient.execute(request);
        } catch (Exception e) {
            System.out.println(e);
        }
        return null;
    }*/

    private HttpResponse create(Payment payment) {
        HttpClient httpClient = HttpClientBuilder.create().build();
        try {
            HttpPost request = new HttpPost(Paths.webTarget + Paths.payment);
            StringEntity params = new StringEntity("{" +
                    "\"" + Constants.oderId + "\": \"" + payment.getOrderId() + "\"" +
                    "\"" + Constants.amount + "\": " + payment.getAmount() +
                    "\"" + Constants.callbackURL + "\": \"" + payment.getCallBack() + "\"" +
                    "} ");
            request.setEntity(params);
            request.addHeader("content-type", "application/json");
            request.addHeader(Constants.apiKey, Constants.ApiKey);
            request.addHeader(Constants.sandBox, String.valueOf(true));
            return httpClient.execute(request);
        } catch (Exception e) {
            System.out.println(e);
        }
        return null;
    }

    public boolean callback(OnlinePaymentRsBodyDTO dto) {
        if (dto.getStatus() != 10) {
            System.out.println("Failed");
            return false;
        }
        Optional<Payment> optional = paymentRepo.findById(dto.getOrderId());
        if (optional.isPresent()) {
            Payment payment = optional.get();
            if (payment.getId().equals(dto.getId())) {
                System.out.println("Failed");
                return false;
            }
            if (payment.getAmount() != dto.getAmount()) {
                System.out.println("Amount is not the same.");
            }
            payment.setTrackId(dto.getTrackId());
        }
        Payment payment = paymentRepo.getOne(dto.getOrderId());
        ;
        return verify(payment);
    }

    public Payment register(Payment payment) {
        return this.paymentRepo.save(payment);
    }

    public boolean verify(Payment payment) {
        HttpResponse verifyResponse = postVerify(modelMapper.map(payment, VerifyRqDTO.class));
        if (verifyResponse.getStatusLine().getStatusCode() == 200) {
//            checkDoubleSpending(verifyResponse);
            System.out.println("The payment has been Successful");
            payment.setFinished(true);
            return true;
        } else {
            System.out.println("Failed Verifying, The money will be deposited to buyer.");
            payment.setFinished(false);
            return false;
        }
    }

    private HttpResponse postVerify(VerifyRqDTO dto) {
        HttpClient httpClient = HttpClientBuilder.create().build();
        try {
            HttpPost request = new HttpPost(Paths.webTarget + Paths.verify);
            StringEntity params = new StringEntity("{" +
                    "\"" + Constants.id + "\": \"" + dto.getId() + "\"" +
                    "\"" + Constants.oderId + "\": \"" + dto.getOrderId() + "\"" +
                    "} ");
            request.setEntity(params);
            request.addHeader("content-type", "application/json");
            request.addHeader(Constants.apiKey, Constants.ApiKey);
            request.addHeader(Constants.sandBox, String.valueOf(true));
            return httpClient.execute(request);
        } catch (Exception e) {
            System.out.println(e);
        }
        return null;
    }

    public Object findAllPayment() {
        return paymentRepo.findAll();
    }
}
