package ir.reedbees.rozakindergarten.module.payment.DTO;


import lombok.Getter;
import lombok.Setter;

import java.sql.Timestamp;

@Getter
@Setter
public class VerifyRqDTO {
    private int status;
    private int trackId;
    private String id;
    private String orderId;
    private int amount;
    private Timestamp date;

}
