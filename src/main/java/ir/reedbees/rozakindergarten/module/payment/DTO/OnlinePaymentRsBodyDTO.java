package ir.reedbees.rozakindergarten.module.payment.DTO;


import lombok.Getter;
import lombok.Setter;

import java.sql.Timestamp;

@Getter
@Setter
public class OnlinePaymentRsBodyDTO {
    private int status;
    private int trackId;
    private String id;
    private Long orderId;
    private int amount;
    private Timestamp date;
}
