package ir.reedbees.rozakindergarten.module.payment.DTO;


import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class PaymentRsBodyDTO {
    private String id;
    private String link;
}
