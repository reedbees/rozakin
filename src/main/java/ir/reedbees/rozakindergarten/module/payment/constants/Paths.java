package ir.reedbees.rozakindergarten.module.payment.constants;

public class Paths {
	public static final String webTarget = "https://api.idpay.ir/v1.1";
	public static final String payment = "/payment";
	public static final String verify = "/payment/verify";
	public static final String callback = "https://rosakindergarten.ir/callback";

}