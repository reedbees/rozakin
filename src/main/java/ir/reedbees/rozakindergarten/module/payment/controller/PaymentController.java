package ir.reedbees.rozakindergarten.module.payment.controller;


import ir.reedbees.rozakindergarten.module.parent.entity.Parent;
import ir.reedbees.rozakindergarten.module.payment.DTO.OnlinePaymentRsBodyDTO;
import ir.reedbees.rozakindergarten.module.payment.DTO.PayCreateRqDTO;
import ir.reedbees.rozakindergarten.module.payment.entity.Payment;
import ir.reedbees.rozakindergarten.module.payment.service.PaymentService;
import ir.reedbees.rozakindergarten.module.users.entity.Kids;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

@Controller
@RequestMapping(value = "/payment")
public class PaymentController {
    private PaymentService paymentService;

    @Autowired
    public PaymentController(PaymentService paymentService) {
        this.paymentService = paymentService;
    }

    /*@RequestMapping("/create")
    public void pay(@RequestBody PayCreateRqDTO dto) throws IOException, JSONException {
        paymentService.pay(dto);
    }*/

    @RequestMapping("/create")
    public String pay(@RequestBody Payment payment, Model model) throws IOException, JSONException {
        if(paymentService.pay(payment).equals("false")){
            return "PayFailed";
        } else {
            model.addAttribute("link",payment.getLink());
            return "Pay";
        }
    }

    @RequestMapping("/callback")
    public String callback(@RequestBody OnlinePaymentRsBodyDTO dto){
        if(!paymentService.callback(dto)){
            return "PayFailed";
        } else {
            return "test";
        }
    }

    @RequestMapping(value = "/reg",method = RequestMethod.POST)
    public String register(@ModelAttribute Payment payment) {
        Payment payment2 ;
        //payment.setFinished(true);
        //payment.setCash(true);
        LocalDate today2 = LocalDate.now();
        //payment2 = (Payment) payment;
        //payment.setDays(today2.plusMonths(1));
        //payment.setKids(kids);
        //paymentService.register(payment);
        System.out.println("this page is for register by hand");
        return "admin";
    }


}
