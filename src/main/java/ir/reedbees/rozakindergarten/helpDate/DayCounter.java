package ir.reedbees.rozakindergarten.helpDate;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

@Getter
@Setter
public class DayCounter {
    private LocalDate localDate;
    private long dayBetween;

    public long dayCount(LocalDate localDate1) {
        localDate = LocalDate.now();
        dayBetween = ChronoUnit.DAYS.between(localDate,localDate1);
        return dayBetween;
    }
}
