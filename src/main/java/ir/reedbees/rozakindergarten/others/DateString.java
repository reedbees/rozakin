package ir.reedbees.rozakindergarten.others;

import java.sql.Timestamp;

public class DateString {
    private Timestamp timestamp;

    public DateString(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return
                "" + timestamp;
    }
}
